#include "AFN.h"
#include <fstream>
#include <iostream>

std::vector<std::string> split(std::string input, std::string delimiter = "X")
{
	std::vector<std::string> result;

	size_t pos = 0;
	std::string token;
	while ((pos = input.find(delimiter)) != std::string::npos)
	{
		token = input.substr(0, pos);
		result.push_back(token);
		input.erase(0, pos + delimiter.length());
	}
	result.push_back(input);
	return result;
}

void AFN::Citire() {
	std::ifstream fileInput("InputFile.txt");
	while () 
	{
		std::string stare;
		fileInput >> stare;
		this->Stari.push_back(stare);
	}

	int nrLitere;
	fileInput >> nrLitere;
	while (nrLitere) {
		std::string litera;
		fileInput >> litera;
		this->Vocabular.push_back(litera);
		--nrLitere;
	}

	fileInput >> this->StareInitiala;

	int nrStariFinale;
	fileInput >> nrStariFinale;
	while (nrStariFinale) {
		std::string stare;
		fileInput >> stare;
		this->StariFinale.push_back(stare);
		--nrStariFinale;
	}

	std::string tranzitieLine;
	fileInput.get(); // lost \n from last line
	while (std::getline(fileInput, tranzitieLine)) {
		std::vector<std::string> tranzitii = split(tranzitieLine);
		std::tuple<std::string, std::string> stareCurenta = std::make_tuple(tranzitii[0], tranzitii[1]);
		this->Tranzitii[stareCurenta] = tranzitii[2];
	}
}

void AFN::Afisare() {
	std::cout << "M={ Multime stari:( ";
	for (int index = 0; index < Stari.size(); index++)
	{
		if (index + 1 != Stari.size())
			std::cout << Stari[index] << " , ";
		else
			std::cout << Stari[index];
	}
	std::cout << " ) , \n\tAlfabet:( ";
	for (int index = 0; index < Vocabular.size(); index++)
	{
		if (index + 1 != Vocabular.size())
			std::cout << Vocabular[index] << " , ";
		else
			std::cout << Vocabular[index];
	}
	std::cout << " ) , \n\tStare Initiala: " << StareInitiala << " , \n\tStari Finale:( ";
	for (int index = 0; index < this->StariFinale.size(); index++)
	{
		if (index + 1 != StariFinale.size())
			std::cout << StariFinale[index] << " , ";
		else
			std::cout << StariFinale[index];
	}
	std::cout << " ) , \n\tMultime Tranzitii:( ";
	for (auto elem : this->Tranzitii) {
		std::cout << "(" << std::get<0>(elem.first) << ", " << std::get<1>(elem.first) << ") -> " << elem.second << ",\n\t\t\t";
	}
	std::cout << " ) \n}.";

}

bool AFN::Verificare(std::string cuvant) {
	std::string stareCurenta = this->StareInitiala;

	for (char litera : cuvant) {
		std::string literaString(1, litera);

		std::tuple<std::string, std::string> pereche = std::make_tuple(stareCurenta, literaString);
		stareCurenta = this->Tranzitii[pereche];

		if (stareCurenta == "$") {
			std::cout << "BLOCAJ!";
			return false;
		}
	}

	for (auto stare : this->StariFinale) {
		if (stareCurenta == stare) {
			return true;
		}
	}

	return false;
}
