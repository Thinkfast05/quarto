#pragma once
#include <vector>
#include <string>
#include <map>

class AFN
{
public:
	void Citire();
	void Afisare();
	bool Verificare(std::string cuvant);

private:
	std::vector<std::string> Stari;
	std::vector<std::string> StariFinale;
	std::string StareInitiala;
	std::vector<std::string> Vocabular;

	std::map<std::tuple<std::string, std::string>, std::vector<std::string>> Tranzitii;
};

