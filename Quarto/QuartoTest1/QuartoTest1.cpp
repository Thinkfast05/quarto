#include "pch.h"
#include "CppUnitTest.h"
#include "../Quarto/Piece.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoTest1
{
	TEST_CLASS(QuartoTest1)
	{
	public:
		
		TEST_METHOD(PieceConstructor)
		{
			Piece piece1(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Full);
			Assert::IsTrue(Piece::BodyStyle::Full == piece1.getBodyStyle());
			Assert::IsTrue(Piece::Colour::Black==piece1.getColour());
		}
	};
}