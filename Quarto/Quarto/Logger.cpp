#include "Logger.h"
#include <iostream>
Logger::Logger(std::ostream& m_logStream, Level level)
	:m_logStream(m_logStream),m_Level(level)
{
}

void Logger::Log(const std::string& message, Level level)
{
	if(level>=m_Level)
	m_logStream << message<<"\n";
}
