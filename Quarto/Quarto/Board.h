#pragma once
#include <array>
#include "Piece.h"
#include <optional>
class Board
{
public:
	using Position = std::pair<uint8_t, uint8_t>;
	Piece &operator [](const Position&);
	const Piece &operator [](const Position&) const;
	friend std::ostream& operator<<(std::ostream& print, const Board&);
private:
	std::array<Piece, 16> m_board;
};