#include "UnusedPieces.h"

UnusedPieces::UnusedPieces()
{

}

Piece UnusedPieces::choosePiece(const std::string& key)
{
	auto extracted = m_unusedPieces.extract(key);
	if (extracted)
		return std::move(extracted.mapped());
	else
		throw "Piece not found.";

		
}

void UnusedPieces::Initialize()
{
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::Black, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Round, Piece::Height::Tall, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Short, Piece::BodyStyle::Hollow));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Full));
	this->Emplace(Piece(Piece::Colour::White, Piece::Shape::Square, Piece::Height::Tall, Piece::BodyStyle::Hollow));
}

void UnusedPieces::Emplace(const Piece& piece)
{
	std::stringstream ss;
	ss << piece;
	m_unusedPieces.insert(std::make_pair(ss.str(), piece));
}

std::ostream& operator<<(std::ostream& output, const UnusedPieces& up)
{
	for (auto unusedPieces : up.m_unusedPieces)
		output << unusedPieces.first << " ";
	output<<"\n";
	return output;
}
