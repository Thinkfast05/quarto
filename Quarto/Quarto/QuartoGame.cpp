#include "QuartoGame.h"
#include <string>
void QuartoGame::Run()
{
	Board board;
	std::string name;
	std::cout << "Introduceti numele pentru player1:";
	std::cin >> name;
	Player player1(name);
	std::cout << "\n";
	std::cout << "Introduceti numele pentru player2:";
	std::cin >> name;
	Player player2(name);
	std::cout << "\n";
	UnusedPieces unused_pieces;
	std::reference_wrapper<Player> picking_player=player1;
	std::reference_wrapper<Player> placeing_player=player2;
	while (true)
	{
		system("cls");
		std::cout << board;
		std::cout << "\n";
		std::cout << "Piese disponibile:\n";

		std::cout << unused_pieces;
		std::cout << "\n";
		std::cout << placeing_player << " trebuie sa aleaga o piesa";
		Piece piece;
		try
		{
			std::string piece_name;
			std::cin >> piece_name;
			piece = unused_pieces.choosePiece(piece_name);
		}
		catch (const char* mesaj)
		{
			std::cout << mesaj;
		}
		int line, column;
		std::cout << picking_player << " trebuie sa aleaga o pozitie";
		std::cin >> line >> column;
		try
		{
			board[{line, column}] = std::move(piece);
		}
		catch (const char* mesaj)
		{
			std::cout << mesaj;
		}
		std::swap(picking_player, placeing_player);
	}
}
