#include "Player.h"
#include <iostream>
Player::Player(std::string name)
	:name(name)
{
}

std::ostream& operator<<(std::ostream& output, const Player& player1)
{
	output << player1.name;
	return output;
}
