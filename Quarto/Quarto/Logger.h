#pragma once
#include <ostream>
#include <string>
#ifdef LOGGING_EXPORTS
#define LOGGING_API _declspec(dllexport)
#else
#define LOGGING_API _declspec(dllimport)
#endif


class LOGGING_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};
	Logger(std::ostream& m_logStream, Level level=Level::Info);
	void Log(const std::string& message, Level level);
private:
	std::ostream& m_logStream;
	Level m_Level;
};

