#pragma once
#include <string>
class Player
{
public:
	Player(std::string);
	friend std::ostream& operator<<(std::ostream&, const Player&);
private:
	std::string name;

};

