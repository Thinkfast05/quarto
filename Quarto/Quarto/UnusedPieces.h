#pragma once
#include <unordered_map>
#include<sstream>
#include "Piece.h"
class UnusedPieces
{
	public:
		UnusedPieces();
		Piece choosePiece(const std::string&);
		friend std::ostream& operator<<(std::ostream& output, const UnusedPieces& up);
	private:
		std::unordered_map<std::string, Piece> m_unusedPieces;
		void Initialize();
		void Emplace(const Piece&);
};

