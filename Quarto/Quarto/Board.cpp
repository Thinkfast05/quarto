#include "Board.h"
#include <tuple>

Piece &Board::operator[](const Position& positionReceived)
{
    uint8_t line=positionReceived.first, column=positionReceived.second;
    return m_board[line*4+column];
}

const Piece &Board::operator[](const Position& positionReceived) const
{
    uint8_t line = positionReceived.first, column = positionReceived.second;
    if (line >= 4 || line <= 0 || column >= 4 || column <= 0)
        catch("Pozitie invalida");
    return m_board[line * 4 + column];
}

std::ostream& operator<<(std::ostream& print, const Board& m_board)
{
    Board::Position p;
    uint8_t line = p.first, column = p.second;
    for (line = 0; line < 4; line++)
    {
        for (column = 0; column < 4; column++)
        {
            print << m_board[p]<<" ";
        }
        print << "\n";
    }
    return print;
}
